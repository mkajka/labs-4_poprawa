﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component2B
{
    public class Komponent2B : AbstractComponent, Interfejs
    {
        public Komponent2B()
        {
            this.RegisterProvidedInterface(typeof(Interfejs), this);
        }
        
        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }

        public void Metoda1()
        {
            Console.WriteLine("Metoda1 - Komponent2B");
        }

        public void Metoda2()
        {
            Console.WriteLine("Metoda2 - Komponent2B");
        }
    }
}
