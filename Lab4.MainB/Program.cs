﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Component1;
using Lab4.Component2B;
using Lab4.Contract;


namespace Lab4.MainB
{
    class Program
    {
        static void Main(string[] args)
        {
            IContainer Kontener = new Container();
            Komponent1 KontenerKomponent1 = new Komponent1();
            Komponent2B KontenerKomponent2B = new Komponent2B();
            Kontener.RegisterComponent(KontenerKomponent1);
            Kontener.RegisterComponent(KontenerKomponent2B);
            Kontener.GetInterface<Interfejs>().Metoda1();
            Console.ReadKey();
        }
    }
}
