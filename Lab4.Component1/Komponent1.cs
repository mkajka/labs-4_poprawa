﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component1
{
    public class Komponent1 :AbstractComponent
    {
        Interfejs interfejs;

        public Komponent1()
        {
            this.RegisterRequiredInterface(typeof(Interfejs));
        }

        public override void InjectInterface(Type type, object impl)
        {
            if (typeof(Interfejs) == type)
            {
                interfejs = (Interfejs)impl;
            }
        }
    }
}
