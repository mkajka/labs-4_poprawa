﻿using System;
using System.Collections.Generic;
using System.Linq;
using ComponentFramework;
using System.Text;

namespace Lab4.Component1
{
    public class Class1 : AbstractComponent, IComponent
    {
        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }

        T IComponent.GetInterface<T>()
        {
            throw new NotImplementedException();
        }

        object IComponent.GetInterface(Type type)
        {
            throw new NotImplementedException();
        }

        void IComponent.InjectInterface<T>(object impl)
        {
            throw new NotImplementedException();
        }

        void IComponent.InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }

        IEnumerable<Type> IComponent.ProvidedInterfaces
        {
            get { throw new NotImplementedException(); }
        }

        IEnumerable<Type> IComponent.RequiredInterfaces
        {
            get { throw new NotImplementedException(); }
        }
    }
}
