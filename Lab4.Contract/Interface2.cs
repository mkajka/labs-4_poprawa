﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Lab4.Contract
{
    public interface IPotrzebneProdukty
    {
        string WybierzProdukt();
        void DostepneProdukty();
        void WybranyProdukt();
    }
}
