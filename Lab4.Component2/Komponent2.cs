﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ComponentFramework;
using Lab4.Contract;

namespace Lab4.Component2
{
    public class Komponent2 : AbstractComponent, Interfejs
    {
        public Komponent2()
        {
            this.RegisterProvidedInterface(typeof(Interfejs), this);
        }
        
        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }

        public void Metoda1()
        {
            Console.WriteLine("Metoda1 - Komponent2");
        }

        public void Metoda2()
        {
            Console.WriteLine("Metoda2 - Komponent2");
        }
    }
}
