﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab4.Contract;
using ComponentFramework;

namespace Lab4.Component2
{
    public class Class1 : AbstractComponent, Interface1
    {
        public string WybierzOpcje()
        {
            return "";
        }

        public void WybranaOpcja()
        {
            Console.WriteLine("");
        }

        public void DostepneOpcje()
        {
            Console.WriteLine("1 2 3");
        }

        public IComponent Zrzutuj(object o)
        {
            return (IComponent)o;
        }

        public override void InjectInterface(Type type, object impl)
        {
            throw new NotImplementedException();
        }
    }
}
